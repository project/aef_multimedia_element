<?php

/**
 * @file
 * Multimedia elements formatter hooks and callbacks.
 */

function theme_aef_multimedia_element_formatter_multimedia_element_embedded($element) {
	// Inside a View this function may be called with null data.  In that case,
	// just return.
	if (empty($element['#item'])) {
		return '';
	}

	$html = "";

	if($node = node_load($element['#item']['nid']))
	{
		node_view($node);
		$html = $node->body;
	}


	return $html;
}

function theme_aef_multimedia_element_formatter_multimedia_element_wide($element) {
	// Inside a View this function may be called with null data.  In that case,
	// just return.
	if (empty($element['#item'])) {
		return '';
	}

	$html = "";

	if($node = node_load($element['#item']['nid']))
	{
		$node = node_build_content($node, false, false);
		//Override the theme
		$node->content['#theme'] = 'node_element_multimedia_block_wide';
		$html = drupal_render($node->content);
	}

	return $html;
}

function theme_aef_multimedia_element_formatter_multimedia_element_wide_2($element) {
	// Inside a View this function may be called with null data.  In that case,
	// just return.
	if (empty($element['#item'])) {
		return '';
	}

	$html = "";

	if($node = node_load($element['#item']['nid']))
	{
		$node = node_build_content($node, false, false);
		//Override the theme
		$node->content['#theme'] = 'node_element_multimedia_block_wide_2';
		$html = drupal_render($node->content);
	}

	return $html;
}

