

function Import(aSrc) {
   document.write('<scr'+'ipt type="text/javascript" src="' + aSrc + '"></sc' + 'ript>');
}

var oEditor		= window.parent.InnerDialogLoaded() ;
var FCK			= oEditor.FCK ;
var FCKLang		= oEditor.FCKLang ;
var FCKConfig	= oEditor.FCKConfig ;
var FCKTools = oEditor.FCKTools ;

Import(FCKConfig.FullBasePath + 'dialog/common/fck_dialog_common.js');
//Test IE6
var IE6 = false;

var strChUserAgent = navigator.userAgent;
var intSplitStart = strChUserAgent.indexOf("(",0);
var intSplitEnd = strChUserAgent.indexOf(")",0);
var strChMid = strChUserAgent.substring(intSplitStart, intSplitEnd);

if(strChMid.indexOf("MSIE 6") != -1) IE6 = true;




// Get the selected map (if available).
var oFakeEltmm = FCK.Selection.GetSelectedElement() ;
var oParsedEltmm ;



if ( oFakeEltmm )
{
	if ( oFakeEltmm.getAttribute( 'EltmmNumber' ) )
	{
		oParsedEltmm = FCK.EltmmHandler.getEltmm( oFakeEltmm.getAttribute( 'emid' ) );
	}
	else
		oFakeEltmm = null ;
}
if ( !oParsedEltmm )
		oParsedEltmm = FCK.EltmmHandler.createNew() ;


window.onload = function()
{
/**
	// Translate the dialog box texts.
	oEditor.FCKLanguageManager.TranslatePage(document) ;
	var btn = GetE('btnxxx') ;
	btn.alt = btn.title = FCKLang.xxxx ;
*/

        // Load the current list of multimedia elements
        LoadMultimediaElementList();

	// Load the selected element information (if any).
	LoadSelection() ;


	//Configure events
	ConfigureEvents() ;



	window.parent.SetOkButton( true ) ;

	if (window.parent.Sizer) window.parent.SetAutoSize( true ) ; 
} ;

function ConfigureEvents()
{
	GetE('txtEltmmNumber').onchange  = UpdatePreview;
	GetE('cmbTheme').onchange  = UpdatePreview;
}

function LoadMultimediaElementList()
{
	//Add a dummy option
	var elt = document.createElement('option');
	elt.setAttribute('value', 0);
	elt.innerHTML = "Current/None";
	GetE('txtEltmmNumber').appendChild(elt);


  var doc = top.document;
  //Look recursively on embedded edits, if any
  var embedded_edit_wrapper = doc.getElementById('aef-embedded-edit-area');
  var embedded_edit = doc.getElementById('aef-embedded-edit-area-iframe');
  while(embedded_edit != undefined && embedded_edit_wrapper != undefined &&
    embedded_edit_wrapper.style.display != 'none')
  {
    embedded_edit_doc = embedded_edit.contentWindow.document;
    embedded_edit_inputs = embedded_edit_doc.getElementsByTagName('input');
    if(embedded_edit_inputs.length == 0)
      break;
    doc = embedded_edit_doc;
    embedded_edit_wrapper = doc.getElementById('aef-embedded-edit-area');
    embedded_edit = doc.getElementById('aef-embedded-edit-area-iframe');
  }

  //Get the inputs
	var inputs = doc.getElementsByTagName('input');
	var regex = new RegExp('\\baef_multimedia_elt\\b');
	var number_regex = new RegExp('\\d+');
	for(var i = 0; i < inputs.length; i++)
	{
		if(regex.test(inputs[i].className) && inputs[i].value != '' && inputs[i].value != undefined)
		{
			var elt = document.createElement('option');
			elt.setAttribute('value', parseInt(number_regex.exec(inputs[i].name)) + 1);
			elt.innerHTML = inputs[i].value;
			GetE('txtEltmmNumber').appendChild(elt);
		}
	}

  //Also load the formatter list. Search in all the embedded-edit iframe (frontpage can be a non-edit page)
  var doc = top.document;
  var embedded_edit = doc.getElementById('aef-embedded-edit-area-iframe');
  var elt = doc.getElementById('aef_multimedia_element_formatters');
  while(elt == undefined && embedded_edit != undefined)
  {
    doc = embedded_edit.contentWindow.document;
    elt = doc.getElementById('aef_multimedia_element_formatters');
    embedded_edit = doc.getElementById('aef-embedded-edit-area-iframe');
  }
  if(elt != undefined)
  {
    var names = elt.getAttribute('formatters_name').split(';');
    var ids = elt.getAttribute('formatters_id').split(';');
    for(var i = 0; i < names.length; i++)
    {
			var elt = document.createElement('option');
			elt.setAttribute('value', ids[i]);
			elt.innerHTML = names[i];
			GetE('cmbTheme').appendChild(elt);
    }
  }
}

function LoadSelection()
{
	emList = GetE('txtEltmmNumber').options;
	for(var i = 0; i < emList.length; i++)
	{
		var matches = emList[i].innerHTML.match(/\[nid:(\d+)\]/);
		if(matches != null && matches[1] == oParsedEltmm.number)
		{
			GetE('txtEltmmNumber').value = i;
		}
	}

	//GetE('txtEltmmNumber').value = oParsedEltmm.number;
	GetE('cmbOrientation').value = oParsedEltmm.orientation;
	GetE('cmbTheme').value = oParsedEltmm.theme;

	UpdatePreview();


}

//#### The OK button was hit.
function Ok()
{

	oEditor.FCKUndo.SaveUndoStep() ;
	
	if(GetE('txtEltmmNumber').options.length > 0)
	{
		var matches =  GetE('txtEltmmNumber').options[GetE('txtEltmmNumber').selectedIndex].innerHTML.match(/\[nid:(\d+)\]/);
		if(matches != null)
			oParsedEltmm.number = matches[1] ;
	}
	oParsedEltmm.orientation = GetE('cmbOrientation').value;
	oParsedEltmm.theme = GetE('cmbTheme').value;

	//If we didn't caught a number, it's likely we didn't choose an EM
	if(oParsedEltmm.number <= 0)
		return true;

	if ( !oFakeEltmm )
		oFakeEltmm = oParsedEltmm.createHtmlElement() ;

	oParsedEltmm.updateHTMLElement(oFakeEltmm);

	//Save the element, for later use
	FCK.EltmmHandler.saveEltmm(oParsedEltmm);
	
	return true ;
}




function UpdatePreview()
{
	if(GetE('txtEltmmNumber').selectedIndex >= 0)
	{
    var formatter = GetE('cmbTheme').options[GetE('cmbTheme').selectedIndex].value;
		var nid = 0;
		var matches = GetE('txtEltmmNumber').options[GetE('txtEltmmNumber').selectedIndex].innerHTML.match(/\[nid:(\d+)\]/);
		if(matches != null)
			nid = matches[1];
		else if(oParsedEltmm != null)
			nid = oParsedEltmm.number;

		if(nid > 0)
		{
			if(IE6 == false)
			  document.getElementById('preview').contentWindow.document.body.innerHTML = '<html><body><div align="center" style="padding-top:170px;"><img src="loading.gif"></div></body></html>';

			//Just remove a few letters from the end of the URL so that it is used as a Drupal URL
			//Fortunately; ?q=url override the clean URL... otherwise no way to get the base URL !!!
			document.getElementById('preview').src= window.location.href.substr(0, window.location.href.length - 2) + "?q=/node/" + nid + "/formatter_notheme/" + formatter;
		}
		else
		{
			//New element
			document.getElementById('preview').contentWindow.document.body.innerHTML = '<html><body><div align="center">Please select a multimedia element in the list. If no elements are present, you need to add some to a nodereference field first.</div><br /></body></html>';
		}

	}
}

function UpdateDimensions()
{

}



