/**

 FckEditor plugin for the multimedia elements.
 Directly interacting with the node edit page (loading of node ids), and AJAX calls to render the elements.




*/




// Toolbar button

// Register the related command.
FCKCommands.RegisterCommand( 'elements_multimedia', new FCKDialogCommand( 'elements_multimedia', "Multimedia element", FCKPlugins.Items['elements_multimedia'].Path + 'dialog/ElementMultimedia.html', 650, 628 ) ) ;

// Create the "elements_multimedia" toolbar button.
var eltMmIcon = new FCKToolbarButton( 'elements_multimedia', 'lala',  "Insert/edit a multimedia element") ;
eltMmIcon.IconPath = FCKPlugins.Items['elements_multimedia'].Path + 'konverter.png' ;

FCKToolbarItems.RegisterItem( 'elements_multimedia', eltMmIcon ) ;


// Drupal FckEditor specific code
// Add the toolbar element to some toolbars
addToolbarElement('elements_multimedia', 'Default', 0);
addToolbarElement('elements_multimedia', 'DrupalFull', 0);
//addToolbarElement('elements_multimedia', 'DrupalBasic', 0);
addToolbarElement('elements_multimedia', 'RfiFull', 0);



if (typeof FCKCommentsProcessor === 'undefined')
{
	var FCKCommentsProcessor = FCKDocumentProcessor.AppendNew() ;
	FCKCommentsProcessor.ProcessDocument = function( oDoc )
	{
		if ( FCK.EditMode != FCK_EDITMODE_WYSIWYG )
			return ;

		if ( !oDoc )
			return ;

	//Find all the comments: <!--{PS..0}-->
	//try to choose the best approach according to the browser:
		if ( oDoc.evaluate )
			this.findCommentsXPath( oDoc );
		else
		{
			if (oDoc.all)
				this.findCommentsIE( oDoc.body ) ;
			else
				this.findComments( oDoc.body ) ;
		}

	}

	FCKCommentsProcessor.findCommentsXPath = function(oDoc) {
		var nodesSnapshot = oDoc.evaluate('//body//comment()', oDoc.body, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null );

		for ( var i=0 ; i < nodesSnapshot.snapshotLength; i++ )
		{
			this.parseComment( nodesSnapshot.snapshotItem(i) ) ;
		}
	}

	FCKCommentsProcessor.findCommentsIE = function(oNode) {
		var aComments = oNode.getElementsByTagName( '!' );
		for(var i=aComments.length-1; i >=0 ; i--)
		{
			var comment = aComments[i] ;
			if (comment.nodeType == 8 ) // oNode.COMMENT_NODE) 
				this.parseComment( comment ) ;
		}
	}

	// Fallback function, iterate all the nodes and its children searching for comments.
	FCKCommentsProcessor.findComments = function( oNode ) 
	{
		if (oNode.nodeType == 8 ) // oNode.COMMENT_NODE) 
		{
			this.parseComment( oNode ) ;
		}
		else 
		{
			if (oNode.hasChildNodes()) 
			{
				var children = oNode.childNodes ;
				for (var i = children.length-1; i >=0 ; i--) 
					this.findComments( children[ i ] );
			}
		}
	}

	// We get a comment node
	// Check that it's one that we are interested on:
	FCKCommentsProcessor.parseComment = function( oNode )
	{
		var value = oNode.nodeValue ;

		// Difference between 2.4.3 and 2.5
		var prefix = ( FCKConfig.ProtectedSource._CodeTag || 'PS\\.\\.' ) ;

		var regex = new RegExp( "\\{" + prefix + "(\\d+)\\}", "g" ) ;

		if ( regex.test( value ) ) 
		{
			var index = RegExp.$1 ;
			var content = FCKTempBin.Elements[ index ] ;

			// Now call the registered parser handlers.
			var oCalls = this.ParserHandlers ;
			if ( oCalls )
			{
				for ( var i = 0 ; i < oCalls.length ; i++ )
					oCalls[ i ]( oNode, content, index ) ;

			}

		}
	}

	/**
		The users of the object will add a parser here, the callback function gets two parameters:
			oNode: it's the node in the editorDocument that holds the position of our content
			oContent: it's the node (removed from the document) that holds the original contents
			index: the reference in the FCKTempBin of our content
	*/
	FCKCommentsProcessor.AddParser = function( handlerFunction )
	{
		if ( !this.ParserHandlers )
			this.ParserHandlers = [ handlerFunction ] ;
		else
		{
			// Check that the event handler isn't already registered with the same listener
			// It doesn't detect function pointers belonging to an object (at least in Gecko)
			if ( this.ParserHandlers.IndexOf( handlerFunction ) == -1 )
				this.ParserHandlers.push( handlerFunction ) ;
		}
	}
}
/**
	END of FCKCommentsProcessor
	---------------------------
*/




// Check if the comment it's one of our scripts:
var ElementsMultimedia_CommentsProcessorParser = function( oNode, oContent, index)
{
		if ( FCK.EltmmHandler.isEltmmComment( oContent ) )
		{
			var oEltmm = FCK.EltmmHandler.createNew() ;
			oEltmm.parse( oContent ) ;
			oEltmm.createHtmlElement( oNode, index ) ;

			//Save the element, for later use
			FCK.EltmmHandler.saveEltmm(oEltmm);
		}
}

FCKCommentsProcessor.AddParser( ElementsMultimedia_CommentsProcessorParser );






// Object that handles the common functions about all the multimedia elts
FCK.EltmmHandler = {
	// Object to store a reference to each map
	eltmms: {},
	// The unique identifier counter for the Eltmms
	emid_counter : 0,

	saveEltmm: function(eltmm){
		this.eltmms[ eltmm.emid ] = eltmm;
	},

	getEltmm: function(id){
		return this.eltmms[id];
	},


	// This can be called from the dialog
	createNew: function()
	{
		var eltmm = new FCKEltmm() ;

		if ( typeof this.emid_counter == 'undefined' ) 
		{
			this.emid_counter = 0;
		}
		this.emid_counter = this.emid_counter + 1;
		eltmm.emid = this.emid_counter;

		return eltmm;
	},

	isEltmmComment: function(comment)
	{
		return (/Element_Multimedia/).test(comment);
	}


	// Store any previous processor so nothing breaks
//	previousProcessor: FCKXHtml.TagProcessors[ 'img' ] 
}


// Our object that will handle parsing of the script and creating the new one.
var FCKEltmm = function() 
{	//Number is the nid
	this.number = 0 ;
	//Orientation can be left, center, right
	this.orientation = 'left' ;
	//emid is an unique identifier
	this.emid = 0;
	//Theme of the EM
	this.theme = '' ;
}

FCKEltmm.prototype.createHtmlElement = function( oReplacedNode, index)
{
	var oFakeNode = FCK.EditorDocument.createElement( 'IMG' ) ;

	if ( !oReplacedNode )
	{
    		index = FCKTempBin.AddElement( 'lala' ) ;
		var prefix = ( FCKConfig.ProtectedSource._CodeTag || 'PS..' ) ;
		oReplacedNode = FCK.EditorDocument.createComment( '{' + prefix + index + '}' ) ;
		FCK.InsertElement(oReplacedNode);
	} 
	oFakeNode.contentEditable = false ;
//--	oFakeNode.setAttribute( '_fckfakelement', 'true', 0 ) ;

	oFakeNode.setAttribute( '_fckrealelement', FCKTempBin.AddElement( oReplacedNode ), 0 ) ;
	oFakeNode.setAttribute( '_fckBinNode', index, 0 ) ;

	oFakeNode.src = FCKPlugins.Items['elements_multimedia'].Path + 'konverter_big.png';
	oFakeNode.style.padding = '10px' ;
	oFakeNode.style.display = 'block' ;
	oFakeNode.style.border = '1px solid black' ;
//	oFakeNode.style.background = 'white center center url("' + FCKPlugins.Items['googlemaps'].Path + 'images/maps_res_logo.png' + '") no-repeat' ;

	// To avoid it to be resized.
	/**oFakeNode.onresizestart = function()
	{
		FCK.EditorWindow.event.returnValue = false ;
		return false ;
	}*/


	oFakeNode.setAttribute("EltmmNumber", this.number, 0) ;
	oFakeNode.setAttribute("emid", this.emid, 0) ;

	oReplacedNode.parentNode.insertBefore( oFakeNode, oReplacedNode ) ;
	oReplacedNode.parentNode.removeChild( oReplacedNode ) ;

	// dimensions
	this.updateHTMLElement( oFakeNode );

	return oFakeNode ;
}

// Open the dialog on double click.
FCKEltmm.OnDoubleClick = function( tag )
{
	if ( tag.tagName == 'IMG' && tag.getAttribute( 'EltmmNumber' ) )
		FCKCommands.GetCommand( 'elements_multimedia' ).Execute() ;
}

FCK.RegisterDoubleClickHandler( FCKEltmm.OnDoubleClick, 'IMG' ) ;

FCKEltmm.prototype.updateHTMLElement = function( oFakeNode )
{
	if(this.orientation == 'left')
	{
		oFakeNode.style.cssFloat = 'left';
		oFakeNode.style.styleFloat = 'left';
		oFakeNode.style.clear = 'none';
		oFakeNode.style.margin = '10px 10px 10px 0px';
	}
	else if(this.orientation == 'right')
	{
		oFakeNode.style.cssFloat = 'right';
		oFakeNode.style.styleFloat = 'right';
		oFakeNode.style.clear = 'none';
		oFakeNode.style.margin = '10px 0px 10px 10px';
	}
	else if(this.orientation == 'center')
	{
		oFakeNode.style.cssFloat = null;
		oFakeNode.style.styleFloat = null;
		oFakeNode.style.clear = 'both';
		oFakeNode.style.margin = '10px 10px 10px 300px';
	}

}

/**
 * Load a multimedia element properties from a comment
 */
FCKEltmm.prototype.parse = function(comment)
{
	var elts = comment.split(/\s*:\s*/);
	this.number = elts[2];
	this.orientation = elts[4];
	this.theme = elts[6];
}


/**
 * When generating the source, remove the placeholders and put a comment, to be handled by a node load hook.
 */
FCKXHtml.TagProcessors.img = function( node, htmlNode, xmlNode )
{
	if ( htmlNode.getAttribute( 'EltmmNumber' ) )
	{
		//Get the stored EltMM
		var oEltmm = FCK.EltmmHandler.getEltmm( htmlNode.getAttribute( 'emid' ) ) ; 
		//Generate the comment
    if(oEltmm.theme != undefined)
  		return xmlNode.ownerDocument.createComment( ':Element_Multimedia:' + oEltmm.number + ':orientation:' + oEltmm.orientation + ':theme:' + oEltmm.theme + ':') ;
    else
      return xmlNode.ownerDocument.createComment( ':Element_Multimedia:' + oEltmm.number + ':orientation:' + oEltmm.orientation + ':') ;
	}

	//Otherwise...
	node = FCKXHtml._AppendChildNodes( node, htmlNode, false ) ;

	return node ;
};
